package com.alice_app.coding_challenge.chess.exception;

public class WrongCommandException extends RuntimeException{

    public WrongCommandException(String message) {
        super(message);
    }

}
