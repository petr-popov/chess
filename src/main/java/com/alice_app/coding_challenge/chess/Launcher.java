package com.alice_app.coding_challenge.chess;

import com.alice_app.coding_challenge.chess.controller.Controller;
import com.alice_app.coding_challenge.chess.model.CheckmateChecker;
import com.alice_app.coding_challenge.chess.model.CommandExecutor;
import com.alice_app.coding_challenge.chess.model.domain.Board;
import com.alice_app.coding_challenge.chess.model.moving.StepChecker;
import com.alice_app.coding_challenge.chess.view.BoardView;
import com.alice_app.coding_challenge.chess.view.Terminal;

public class Launcher {

    public static void main(String[] args) {

        Board board = new Board();
        BoardView boardView = new BoardView(board);
        Terminal terminal = new Terminal();
        StepChecker stepChecker = new StepChecker(board);
        CommandExecutor commandExecutor = new CommandExecutor(board, stepChecker);
        CheckmateChecker checkmateChecker = new CheckmateChecker(board, stepChecker);

        Controller controller = new Controller(terminal,
            boardView,
            commandExecutor,
            checkmateChecker);

        while (!controller.isGameCompleted()) {
            controller.askUserAndProcess();
        }

    }
}
