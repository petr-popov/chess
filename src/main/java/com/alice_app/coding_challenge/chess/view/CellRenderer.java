package com.alice_app.coding_challenge.chess.view;

import com.alice_app.coding_challenge.chess.model.domain.ChessPiece;

class CellRenderer {

    private static final int WIDTH = 10;

    static String[] renderPiece(ChessPiece piece) {
        return new String[] {
            blank(),
            lineWithWord(piece.getColor().toString()),
            lineWithWord(piece.getPieceType().toString()),
            bottomBorder()
        };
    }

    static String[] renderLabel(char label) {
        return new String[] {
            blank(),
            lineWithWord("" + label),
            blank(),
            bottomBorder()
        };
    }

    private static String bottomBorder() {
        return repeat('_', WIDTH - 2) + "|";
    }

    private static String blank() {
        return repeat(' ', WIDTH - 2) + '|';
    }

    private static String lineWithWord(String word) {
        return addSpaces(word) + '|';
    }

    private static String addSpaces(String word) {
        int spacesToAdd = 8 - word.length();
        int leftSpaces = spacesToAdd / 2;
        int rightSpaces = spacesToAdd - leftSpaces;
        return repeat(' ', leftSpaces) + word + repeat(' ', rightSpaces);
    }

    private static String repeat(char symbol, int n) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < n; i++) {
            result.append(symbol);
        }
        return result.toString();
    }
}
