package com.alice_app.coding_challenge.chess.view;

import com.alice_app.coding_challenge.chess.model.domain.Board;
import com.alice_app.coding_challenge.chess.model.CoordinateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static com.alice_app.coding_challenge.chess.view.CellRenderer.renderLabel;

public class BoardView {

    private final Board board;
    private StringBuilder formatted;

    public BoardView(Board board) {
        this.board = board;
    }

    public String formatBoard() {
        formatted = new StringBuilder();
        formatRow(renderLabel(' '),
            column -> renderLabel(CoordinateUtil.toColumnLabel(column)));
        for (int line = Board.SIZE - 1; line >= 0; line--) {
            formatRow(line);
        }
        formatRow(renderLabel(' '), column ->
            renderLabel(CoordinateUtil.toColumnLabel(column)));
        return formatted.toString();
    }

    private void formatRow(final int line) {
        formatRow(
            renderLabel(CoordinateUtil.toRowLabel(line)),
            column -> board.get(column, line)
                .map(CellRenderer::renderPiece)
                .orElse(renderLabel(' ')));
    }

    private void formatRow(String[] labelCell, Function<Integer, String[]> cellsProvider) {
        List<String[]> cells = new ArrayList<>();
        cells.add(labelCell);
        for (int i = 0; i < Board.SIZE; i++) {
            cells.add(cellsProvider.apply(i));
        }
        cells.add(labelCell);
        formatCells(cells);
    }

    private void formatCells(List<String[]> cells) {
        int cellHeight = cells.get(0).length;
        for (int i = 0; i < cellHeight; i++) {
            for (String[] cell : cells) {
                formatted.append(cell[i]);
            }
            formatted.append('\n');
        }
    }

}
