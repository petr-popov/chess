package com.alice_app.coding_challenge.chess.view;

import java.util.Scanner;

public class Terminal {

    private final Scanner scanner;

    public Terminal() {
        scanner = new Scanner(System.in);
    }

    public String readCommand() {
        return scanner.nextLine();
    }

    public void print(String message) {
        System.out.println(message);
    }
}
