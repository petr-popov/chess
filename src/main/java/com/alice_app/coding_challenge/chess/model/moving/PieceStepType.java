package com.alice_app.coding_challenge.chess.model.moving;

public enum PieceStepType {

    FORWARD(0, 1),
    BACKWARD(0, -1),
    LEFT(-1, 0),
    RIGHT(1, 0),

    DIAGONAL_LEFT_FORWARD(-1, 1),
    DIAGONAL_RIGHT_FORWARD(1, 1),
    DIAGONAL_RIGHT_BACKWARD(1, -1),
    DIAGONAL_LEFT_BACKWARD(-1, -1),

    KNIGHT_1(1, 2),
    KNIGHT_2(2, 1),
    KNIGHT_3(-1, 2),
    KNIGHT_4(1, -2),
    KNIGHT_5(-2, 1),
    KNIGHT_6(2, -1),
    KNIGHT_7(-1, -2),
    KNIGHT_8(-2, -1);

    private final int deltaX;
    private final int deltaY;

    PieceStepType(int deltaX, int deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    public int getDeltaX() {
        return deltaX;
    }

    public int getDeltaY() {
        return deltaY;
    }
}
