package com.alice_app.coding_challenge.chess.model;

import com.alice_app.coding_challenge.chess.exception.WrongCommandException;
import com.alice_app.coding_challenge.chess.model.domain.PieceColor;

import static com.alice_app.coding_challenge.chess.model.CoordinateUtil.fromColumnLabel;
import static com.alice_app.coding_challenge.chess.model.CoordinateUtil.fromRowLabel;

public class CommandParser {

    public static Move parse(PieceColor color, String command) {
        command = command.trim();
        String[] coordinates = command.split(" ");
        if (coordinates.length != 2
            || coordinates[0].length() != 2
            || coordinates[1].length() != 2) {
            throw new WrongCommandException("Command should be in 'e2 e4' format");
        }
        char fromX = coordinates[0].charAt(0);
        char fromY = coordinates[0].charAt(1);
        char toX = coordinates[1].charAt(0);
        char toY = coordinates[1].charAt(1);
        return new Move(
            color,
            fromColumnLabel(fromX),
            fromRowLabel(fromY),
            fromColumnLabel(toX),
            fromRowLabel(toY)
        );
    }
}
