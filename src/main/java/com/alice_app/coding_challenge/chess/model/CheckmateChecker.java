package com.alice_app.coding_challenge.chess.model;

import com.alice_app.coding_challenge.chess.model.domain.Board;
import com.alice_app.coding_challenge.chess.model.domain.ChessPiece;
import com.alice_app.coding_challenge.chess.model.domain.PieceColor;
import com.alice_app.coding_challenge.chess.model.moving.StepChecker;

import java.util.Optional;

import static com.alice_app.coding_challenge.chess.model.domain.PieceType.KING;

public class CheckmateChecker {

    private final Board board;
    private final StepChecker stepChecker;

    public CheckmateChecker(Board board, StepChecker stepChecker) {
        this.board = board;
        this.stepChecker = stepChecker;
    }

    public boolean hasCheck(PieceColor player) {
        return hasCheck(player, findKing(player));
    }

    public boolean hasCheckmate(PieceColor player) {
        Coordinate king = findKing(player);
        for (int deltaX = -1; deltaX <= 1; deltaX++) {
            for (int deltaY = -1; deltaY <= 1; deltaY++) {
                int x = king.getX() + deltaX;
                int y = king.getY() + deltaY;
                if (Board.isInsideBoard(x, y) && !hasCheck(player, new Coordinate(x, y))) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean hasCheck(PieceColor player, Coordinate king) {
        for (int x = 0; x < Board.SIZE; x++) {
            for (int y = 0; y < Board.SIZE; y++) {
                Optional<ChessPiece> chessPiece = board.get(x, y);
                if (chessPiece.isPresent()
                    && chessPiece.get().getColor() != player) {
                    ChessPiece piece = chessPiece.get();
                    boolean reachable = stepChecker.isCorrect(
                        piece.getPieceType(),
                        new Move(piece.getColor(), x, y, king.getX(), king.getY()),
                        false);
                    if (reachable) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private Coordinate findKing(PieceColor color) {
        for (int x = 0; x < Board.SIZE; x++) {
            for (int y = 0; y < Board.SIZE; y++) {
                Optional<ChessPiece> chessPiece = board.get(x, y);
                if (chessPiece.isPresent()
                    && chessPiece.get().getPieceType() == KING
                    && chessPiece.get().getColor() == color) {
                    return new Coordinate(x, y);
                }
            }

        }
        throw new IllegalStateException("king is missing");
    }
}
