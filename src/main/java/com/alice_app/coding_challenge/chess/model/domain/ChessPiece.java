package com.alice_app.coding_challenge.chess.model.domain;

public class ChessPiece {

    private final PieceColor pieceColor;
    private final PieceType pieceType;

    public ChessPiece(PieceColor pieceColor, PieceType pieceType) {
        this.pieceColor = pieceColor;
        this.pieceType = pieceType;
    }

    public PieceColor getColor() {
        return pieceColor;
    }

    public PieceType getPieceType() {
        return pieceType;
    }
}
