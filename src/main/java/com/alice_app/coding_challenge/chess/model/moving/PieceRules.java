package com.alice_app.coding_challenge.chess.model.moving;

import com.alice_app.coding_challenge.chess.model.domain.Board;
import com.alice_app.coding_challenge.chess.model.domain.PieceType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.alice_app.coding_challenge.chess.model.domain.PieceType.*;
import static com.alice_app.coding_challenge.chess.model.moving.PieceMove.InteractionType.*;
import static com.alice_app.coding_challenge.chess.model.moving.PieceStepType.*;

/**
 * Definition of possible moves for chess pieces. Used by {@link StepChecker}
 */
class PieceRules {

    private final Map<PieceType, List<PieceMove>> moves = new HashMap<>();

    PieceRules() {
        addMove(PAWN, new PieceMove(FORWARD, 1, 2, ONLY_MOVE));
        addMove(PAWN, new PieceMove(DIAGONAL_LEFT_FORWARD, 1, 1, ONLY_KILL));
        addMove(PAWN, new PieceMove(DIAGONAL_RIGHT_FORWARD, 1, 1, ONLY_KILL));

        addMove(ROOK, new PieceMove(FORWARD, Board.SIZE));
        addMove(ROOK, new PieceMove(BACKWARD, Board.SIZE));
        addMove(ROOK, new PieceMove(LEFT, Board.SIZE));
        addMove(ROOK, new PieceMove(RIGHT, Board.SIZE));

        addMove(KNIGHT, new PieceMove(KNIGHT_1, 1));
        addMove(KNIGHT, new PieceMove(KNIGHT_2, 1));
        addMove(KNIGHT, new PieceMove(KNIGHT_3, 1));
        addMove(KNIGHT, new PieceMove(KNIGHT_4, 1));
        addMove(KNIGHT, new PieceMove(KNIGHT_5, 1));
        addMove(KNIGHT, new PieceMove(KNIGHT_6, 1));
        addMove(KNIGHT, new PieceMove(KNIGHT_7, 1));
        addMove(KNIGHT, new PieceMove(KNIGHT_8, 1));

        addMove(BISHOP, new PieceMove(DIAGONAL_LEFT_FORWARD, Board.SIZE));
        addMove(BISHOP, new PieceMove(DIAGONAL_RIGHT_FORWARD, Board.SIZE));
        addMove(BISHOP, new PieceMove(DIAGONAL_LEFT_BACKWARD, Board.SIZE));
        addMove(BISHOP, new PieceMove(DIAGONAL_RIGHT_BACKWARD, Board.SIZE));

        addMove(QUEEN, new PieceMove(FORWARD, Board.SIZE));
        addMove(QUEEN, new PieceMove(BACKWARD, Board.SIZE));
        addMove(QUEEN, new PieceMove(LEFT, Board.SIZE));
        addMove(QUEEN, new PieceMove(RIGHT, Board.SIZE));
        addMove(QUEEN, new PieceMove(DIAGONAL_LEFT_FORWARD, Board.SIZE));
        addMove(QUEEN, new PieceMove(DIAGONAL_RIGHT_FORWARD, Board.SIZE));
        addMove(QUEEN, new PieceMove(DIAGONAL_LEFT_BACKWARD, Board.SIZE));
        addMove(QUEEN, new PieceMove(DIAGONAL_RIGHT_BACKWARD, Board.SIZE));

        addMove(KING, new PieceMove(FORWARD, 1, 1, MOVE_OR_KILL));
        addMove(KING, new PieceMove(BACKWARD, 1, 1, MOVE_OR_KILL));
        addMove(KING, new PieceMove(LEFT, 1, 1, MOVE_OR_KILL));
        addMove(KING, new PieceMove(RIGHT, 1, 1, MOVE_OR_KILL));
        addMove(KING, new PieceMove(DIAGONAL_LEFT_FORWARD, 1, 1, MOVE_OR_KILL));
        addMove(KING, new PieceMove(DIAGONAL_RIGHT_FORWARD, 1, 1, MOVE_OR_KILL));
        addMove(KING, new PieceMove(DIAGONAL_LEFT_BACKWARD, 1, 1, MOVE_OR_KILL));
        addMove(KING, new PieceMove(DIAGONAL_RIGHT_BACKWARD, 1, 1, MOVE_OR_KILL));
    }

    List<PieceMove> getMoves(PieceType type) {
        return moves.get(type);
    }

    private void addMove(PieceType pieceType, PieceMove move) {
        List<PieceMove> pieceMoves = this.moves.computeIfAbsent(pieceType, k -> new ArrayList<>());
        pieceMoves.add(move);
    }
}
