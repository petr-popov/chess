package com.alice_app.coding_challenge.chess.model.domain;

import java.util.Optional;

import static com.alice_app.coding_challenge.chess.model.domain.PieceType.*;
import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.*;

public class Board {

    public static final int SIZE = 8;

    private static final PieceType[] initialPositions = {
        ROOK, KNIGHT, BISHOP, KING, QUEEN, BISHOP, KNIGHT, ROOK
    };

    private final ChessPiece[][] board = new ChessPiece[SIZE][SIZE];

    public static boolean isInsideBoard(int x, int y) {
        return x < Board.SIZE && y < Board.SIZE && x >= 0 && y >= 0;
    }

    public Board() {
        for (int x = 0; x < SIZE; x++) {
            board[0][x] = new ChessPiece(WHITE, initialPositions[x]);
            board[1][x] = new ChessPiece(WHITE, PAWN);
            board[SIZE - 2][x] = new ChessPiece(BLACK, PAWN);
            board[SIZE - 1][x] = new ChessPiece(BLACK, initialPositions[x]);
        }
    }

    public Optional<ChessPiece> get(int x, int y) {
        return Optional.ofNullable(board[y][x]);
    }

    public void move(int fromX, int fromY, int toX, int toY) {
        board[toY][toX] = board[fromY][fromX];
        board[fromY][fromX] = null;
    }
}
