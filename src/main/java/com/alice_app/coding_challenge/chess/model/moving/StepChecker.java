package com.alice_app.coding_challenge.chess.model.moving;

import com.alice_app.coding_challenge.chess.model.Move;
import com.alice_app.coding_challenge.chess.model.domain.Board;
import com.alice_app.coding_challenge.chess.model.domain.PieceType;
import com.alice_app.coding_challenge.chess.model.domain.PieceColor;


import static com.alice_app.coding_challenge.chess.model.moving.PieceMove.InteractionType.*;

/**
 * Checks is move correct using rules defined in {@link PieceRules}
 */
public class StepChecker {

    private final Board board;
    private final PieceRules rules = new PieceRules();

    public StepChecker(Board board) {
        this.board = board;
    }

    public boolean isCorrect(PieceType type, Move step, Boolean firstMove) {

        boolean kill = board.get(step.getToX(), step.getToY()).isPresent();

        return rules.getMoves(type)
            .stream()
            .filter(move ->
                move.getInteractionType() == MOVE_OR_KILL
                    || kill && move.getInteractionType() == ONLY_KILL
                    || !kill && move.getInteractionType() == ONLY_MOVE)
            .anyMatch(move -> {
                int iterations = firstMove
                    ? move.getFirstMoveIterations()
                    : move.getIterations();

                int x = step.getFromX();
                int y = step.getFromY();
                for (int i = 0; i < iterations; i++) {
                    x += move.getMoveType().getDeltaX();
                    if (step.getPlayerPiecesColor() == PieceColor.WHITE) {
                        y += move.getMoveType().getDeltaY();
                    } else {
                        y -= move.getMoveType().getDeltaY();
                    }
                    if (x == step.getToX() && y == step.getToY()) { //desired coordinates
                        return true;
                    } else if (!Board.isInsideBoard(x, y)
                        || board.get(x, y).isPresent()) {  //faced obstacle
                        return false;
                    }
                }
                return false;
            });
    }
}
