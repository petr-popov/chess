package com.alice_app.coding_challenge.chess.model;

import com.alice_app.coding_challenge.chess.exception.WrongCommandException;
import com.alice_app.coding_challenge.chess.model.domain.Board;
import com.alice_app.coding_challenge.chess.model.domain.ChessPiece;
import com.alice_app.coding_challenge.chess.model.domain.PieceColor;
import com.alice_app.coding_challenge.chess.model.moving.StepChecker;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.alice_app.coding_challenge.chess.model.domain.PieceType.KING;

/**
 * Checks that it is allowed to execute command and moves piece
 * For piece specific checks it uses {@link StepChecker#isCorrect}
 */
public class CommandExecutor {

    private final Board board;
    private final StepChecker stepChecker;
    private final Set<ChessPiece> movedPieces = new HashSet<>();

    public CommandExecutor(Board board, StepChecker stepChecker) {
        this.board = board;
        this.stepChecker = stepChecker;
    }

    public void execute(PieceColor player, String strCommand) {
        Move move = CommandParser.parse(player, strCommand);
        Optional<ChessPiece> pieceOptional = board.get(move.getFromX(), move.getFromY());
        if (pieceOptional.isPresent()) {

            ChessPiece piece = pieceOptional.get();

            checkPieceOwnership(player, piece);

            checkOwnPieceInDestinationCell(player, move);

            checkKingInDestinationCell(move);

            checkThatMoveIsCorrect(move, piece);

            board.move(
                move.getFromX(),
                move.getFromY(),
                move.getToX(),
                move.getToY());

            movedPieces.add(piece);
        } else {
            throw new WrongCommandException("Cell is empty");
        }
    }

    private void checkThatMoveIsCorrect(Move move, ChessPiece piece) {
        boolean correctStep = stepChecker.isCorrect(
            piece.getPieceType(),
            move,
            !movedPieces.contains(piece)
        );

        if (!correctStep) {
            throw new WrongCommandException("Incorrect move");
        }
    }

    private void checkPieceOwnership(PieceColor player, ChessPiece piece) {
        if (player != piece.getColor()) {
            throwIncorrectMove();
        }
    }

    private void checkOwnPieceInDestinationCell(PieceColor player, Move move) {
        board.get(move.getToX(), move.getToY())
            .filter(piece -> piece.getColor() == player)
            .ifPresent(piece -> {
                throw new WrongCommandException("There is your piece in destination cell");
            });
    }

    private void checkKingInDestinationCell(Move move) {
        board.get(move.getToX(), move.getToY())
            .filter(piece -> piece.getPieceType() == KING)
            .ifPresent(piece -> throwIncorrectMove());
    }

    private void throwIncorrectMove() {
        throw new WrongCommandException("Incorrect move");
    }
}
