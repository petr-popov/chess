package com.alice_app.coding_challenge.chess.model;

import com.alice_app.coding_challenge.chess.exception.WrongCommandException;

import java.util.Arrays;

public class CoordinateUtil {

    private static final char[] rowNames = {
        '1', '2', '3', '4', '5', '6', '7', '8'
    };

    private static final char[] columnNames = {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'
    };

    public static char toRowLabel(int coordinate) {
        return rowNames[coordinate];
    }

    public static char toColumnLabel(int coordinate) {
        return columnNames[coordinate];
    }

    public static int fromRowLabel(char label) {
        return convert(label, rowNames);
    }

    public static int fromColumnLabel(char label) {
        return convert(label, columnNames);
    }

    private static int convert(char label, char[] labels) {
        for (int i = 0; i < labels.length; i++) {
            if (label == labels[i]) {
                return i;
            }
        }
        throw new WrongCommandException(
            String.format("Wrong coordinate '%s', valid options: %s",
                label, Arrays.toString(labels))
        );
    }
}
