package com.alice_app.coding_challenge.chess.model.moving;

public class PieceMove {

    public enum InteractionType {
        ONLY_MOVE,
        ONLY_KILL,
        MOVE_OR_KILL
    }

    private final PieceStepType moveType;
    private final InteractionType interactionType;
    private final int iterations;
    private final int firstMoveIterations;

    PieceMove(PieceStepType moveType, int iterations) {
        this(moveType, iterations, iterations, InteractionType.MOVE_OR_KILL);
    }

    PieceMove(PieceStepType moveType,
              int iterations,
              int firstMoveIterations,
              InteractionType interactionType) {
        this.moveType = moveType;
        this.iterations = iterations;
        this.firstMoveIterations = firstMoveIterations;
        this.interactionType = interactionType;
    }

    PieceStepType getMoveType() {
        return moveType;
    }

    InteractionType getInteractionType() {
        return interactionType;
    }

    int getIterations() {
        return iterations;
    }

    int getFirstMoveIterations() {
        return firstMoveIterations;
    }
}
