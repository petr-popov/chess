package com.alice_app.coding_challenge.chess.model;

import com.alice_app.coding_challenge.chess.model.domain.PieceColor;

public class Move {

    private final PieceColor playerPiecesColor;
    private final int fromX;
    private final int fromY;
    private final int toX;
    private final int toY;

    public Move(PieceColor color,
                int fromX,
                int fromY,
                int toX,
                int toY) {
        this.playerPiecesColor = color;
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
    }

    public PieceColor getPlayerPiecesColor() {
        return playerPiecesColor;
    }

    public int getFromX() {
        return fromX;
    }

    public int getFromY() {
        return fromY;
    }

    public int getToX() {
        return toX;
    }

    public int getToY() {
        return toY;
    }
}
