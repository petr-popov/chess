package com.alice_app.coding_challenge.chess.model.domain;

public enum PieceColor {
    WHITE,
    BLACK
}
