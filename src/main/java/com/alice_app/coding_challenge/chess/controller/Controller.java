package com.alice_app.coding_challenge.chess.controller;

import com.alice_app.coding_challenge.chess.exception.WrongCommandException;
import com.alice_app.coding_challenge.chess.model.CheckmateChecker;
import com.alice_app.coding_challenge.chess.model.CommandExecutor;
import com.alice_app.coding_challenge.chess.model.domain.PieceColor;
import com.alice_app.coding_challenge.chess.view.BoardView;
import com.alice_app.coding_challenge.chess.view.Terminal;

import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.BLACK;
import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.WHITE;

public class Controller {

    private final BoardView view;
    private final Terminal terminal;
    private final CommandExecutor commandExecutor;
    private final CheckmateChecker checkmateChecker;

    boolean firstPlayer = true;
    private boolean gameCompleted = false;
    private boolean wrongCommand;

    public Controller(Terminal terminal,
                      BoardView view,
                      CommandExecutor commandExecutor,
                      CheckmateChecker checkmateChecker) {
        this.terminal = terminal;
        this.view = view;
        this.commandExecutor = commandExecutor;
        this.checkmateChecker = checkmateChecker;
    }

    public void askUserAndProcess() {
        if (!wrongCommand) {
            terminal.print(view.formatBoard());
        }
        PieceColor color = firstPlayer ? WHITE : BLACK;

        if (checkmateChecker.hasCheckmate(color)) {
            terminal.print(String.format("Player %s you checkmate\nPlayer %s, congratulations!!!",
                firstPlayer ? 1 : 2,
                firstPlayer ? 2 : 1
            ));
            gameCompleted = true;
            return;
        } else if (checkmateChecker.hasCheck(color)) {
            terminal.print(String.format("Player %s you check", firstPlayer ? 1 : 2));
        }

        terminal.print(String.format("Player %s: ", firstPlayer ? 1 : 2));
        try {
            commandExecutor.execute(color, terminal.readCommand());
            firstPlayer = !firstPlayer;
            wrongCommand = false;
        } catch (WrongCommandException e) {
            terminal.print(e.getMessage());
            wrongCommand = true;
        }
    }

    public boolean isGameCompleted() {
        return gameCompleted;
    }

}
