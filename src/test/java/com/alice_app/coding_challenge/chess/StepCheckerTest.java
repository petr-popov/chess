package com.alice_app.coding_challenge.chess;

import com.alice_app.coding_challenge.chess.model.Move;
import com.alice_app.coding_challenge.chess.model.domain.Board;
import com.alice_app.coding_challenge.chess.model.domain.ChessPiece;
import com.alice_app.coding_challenge.chess.model.moving.StepChecker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static com.alice_app.coding_challenge.chess.model.domain.PieceType.*;
import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.BLACK;
import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.WHITE;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class StepCheckerTest {

    @Mock
    private Board board;

    private StepChecker stepChecker;

    @BeforeEach
    void create() {
        initMocks(this);
        stepChecker = new StepChecker(board);
    }

    @Test
    void testFirstMovePositive() {
        when(board.get(0, 0)).thenReturn(Optional.of(new ChessPiece(WHITE, PAWN)));
        when(board.get(0, 1)).thenReturn(Optional.empty());
        when(board.get(0, 2)).thenReturn(Optional.empty());
        assertTrue(stepChecker.isCorrect(PAWN,
            new Move(WHITE, 0, 0, 0, 2), true));
    }

    @Test
    void testFirstStepNotAllowed() {
        when(board.get(0, 0)).thenReturn(Optional.of(new ChessPiece(WHITE, PAWN)));
        when(board.get(0, 1)).thenReturn(Optional.empty());
        when(board.get(0, 2)).thenReturn(Optional.empty());
        assertFalse(stepChecker.isCorrect(PAWN,
            new Move(WHITE, 0, 0, 0, 2), false));
    }

    @Test
    void testKilling() {
        when(board.get(0, 0)).thenReturn(Optional.of(new ChessPiece(WHITE, PAWN)));
        when(board.get(1, 1)).thenReturn(Optional.of(new ChessPiece(BLACK, PAWN)));
        assertTrue(stepChecker.isCorrect(PAWN,
            new Move(WHITE, 0, 0, 1, 1), true));
    }

    @Test
    void testObstacle() {
        when(board.get(0, 0)).thenReturn(Optional.of(new ChessPiece(WHITE, PAWN)));
        when(board.get(0, 1)).thenReturn(Optional.of(new ChessPiece(WHITE, PAWN)));
        when(board.get(0, 2)).thenReturn(Optional.empty());
        assertFalse(stepChecker.isCorrect(PAWN,
            new Move(WHITE, 0, 0, 0, 2), true));
    }

    @Test
    void testBlackPawnKilling() {
        when(board.get(1, 1)).thenReturn(Optional.of(new ChessPiece(BLACK, PAWN)));
        when(board.get(0, 0)).thenReturn(Optional.of(new ChessPiece(WHITE, PAWN)));
        assertTrue(stepChecker.isCorrect(PAWN,
            new Move(BLACK, 1, 1, 0, 0), true));
    }

    @Test
    void testDiagonalPositive() {
        when(board.get(0, 0)).thenReturn(Optional.of(new ChessPiece(WHITE, BISHOP)));
        when(board.get(1, 1)).thenReturn(Optional.empty());
        when(board.get(2, 2)).thenReturn(Optional.empty());
        assertTrue(stepChecker.isCorrect(BISHOP,
            new Move(WHITE, 0, 0, 2, 2), false));
    }

    @Test
    void testDiagonalObstacle() {
        when(board.get(0, 0)).thenReturn(Optional.of(new ChessPiece(WHITE, BISHOP)));
        when(board.get(1, 1)).thenReturn(Optional.of(new ChessPiece(WHITE, BISHOP)));
        when(board.get(2, 2)).thenReturn(Optional.empty());
        assertFalse(stepChecker.isCorrect(BISHOP,
            new Move(WHITE, 0, 0, 2, 2), false));
    }
}
