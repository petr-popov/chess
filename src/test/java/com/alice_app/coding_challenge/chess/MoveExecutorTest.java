package com.alice_app.coding_challenge.chess;

import com.alice_app.coding_challenge.chess.exception.WrongCommandException;
import com.alice_app.coding_challenge.chess.model.CommandExecutor;
import com.alice_app.coding_challenge.chess.model.domain.Board;
import com.alice_app.coding_challenge.chess.model.domain.ChessPiece;
import com.alice_app.coding_challenge.chess.model.moving.StepChecker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Optional;

import static com.alice_app.coding_challenge.chess.model.CoordinateUtil.fromColumnLabel;
import static com.alice_app.coding_challenge.chess.model.CoordinateUtil.fromRowLabel;
import static com.alice_app.coding_challenge.chess.model.domain.PieceType.*;
import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.BLACK;
import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.WHITE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@SuppressWarnings("OptionalGetWithoutIsPresent")
class MoveExecutorTest {

    @Mock
    private StepChecker stepChecker;

    private Board board;
    private CommandExecutor executor;

    @BeforeEach
    void initCommandExecutor() {
        initMocks(this);
        board = new Board();
        executor = new CommandExecutor(board, stepChecker);

        when(stepChecker.isCorrect(any(), any(), any())).thenReturn(true);
    }

    @Test
    void testMoveSuccess() {
        int x = fromColumnLabel('e');
        int fromY = fromRowLabel('2');
        int toY = fromRowLabel('4');

        ChessPiece atFromBefore = board.get(x, fromY).get();
        executor.execute(WHITE, "e2 e4");
        ChessPiece atToAfter = board.get(x, toY).get();

        Optional<ChessPiece> atFromAfter = board.get(x, fromY);
        assertFalse(atFromAfter.isPresent());
        assertEquals(atFromBefore, atToAfter);
    }

    @Test
    void testMovingPieceOfWrongColor() {
        int x = fromColumnLabel('e');
        int fromY = fromRowLabel('2');
        int toY = fromRowLabel('4');

        board = Mockito.mock(Board.class);
        executor = new CommandExecutor(board, stepChecker);

        when(board.get(x, fromY)).thenReturn(Optional.of(new ChessPiece(WHITE, ROOK)));
        when(board.get(x, toY)).thenReturn(Optional.empty());

        WrongCommandException e = assertThrows(WrongCommandException.class,
            () -> executor.execute(BLACK, "e2 e4"));
        assertEquals("Incorrect move", e.getMessage());
    }

    @Test
    void testKillingKing() {
        int x = fromColumnLabel('e');
        int fromY = fromRowLabel('2');
        int toY = fromRowLabel('4');

        board = Mockito.mock(Board.class);
        executor = new CommandExecutor(board, stepChecker);

        when(board.get(x, fromY)).thenReturn(Optional.of(new ChessPiece(WHITE, ROOK)));
        when(board.get(x, toY)).thenReturn(Optional.of(new ChessPiece(BLACK, KING)));

        WrongCommandException e = assertThrows(WrongCommandException.class,
            () -> executor.execute(WHITE, "e2 e4"));
        assertEquals("Incorrect move", e.getMessage());
    }

    @Test
    void testFirstMoveEvaluation() {
        executor.execute(WHITE, "e2 e3");
        verify(stepChecker).isCorrect(eq(PAWN), any(), eq(true));

        executor.execute(WHITE, "e3 e4");
        verify(stepChecker).isCorrect(eq(PAWN), any(), eq(false));
    }

    @ParameterizedTest
    @CsvSource(value = {
        "e3 e4=Cell is empty",
        "e2 f2=There is your piece in destination cell"
    }, delimiter = '=')
    void testMoveEmptyCell(String command, String errorMessage) {
        WrongCommandException e = assertThrows(WrongCommandException.class, () ->
            executor.execute(WHITE, command));

        assertEquals(errorMessage, e.getMessage());
    }

}
