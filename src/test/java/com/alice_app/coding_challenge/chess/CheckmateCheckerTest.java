package com.alice_app.coding_challenge.chess;

import com.alice_app.coding_challenge.chess.model.CheckmateChecker;
import com.alice_app.coding_challenge.chess.model.domain.Board;
import com.alice_app.coding_challenge.chess.model.domain.ChessPiece;
import com.alice_app.coding_challenge.chess.model.moving.StepChecker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static com.alice_app.coding_challenge.chess.model.domain.PieceType.KING;
import static com.alice_app.coding_challenge.chess.model.domain.PieceType.ROOK;
import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.BLACK;
import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.WHITE;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class CheckmateCheckerTest {

    @Mock
    private Board board;
    private CheckmateChecker checkmateChecker;

    @BeforeEach
    void init() {
        initMocks(this);
        for (int x = 0; x < Board.SIZE; x++) {
            for (int y = 0; y < Board.SIZE; y++) {
                when(board.get(x, y)).thenReturn(Optional.empty());
            }
        }
        checkmateChecker = new CheckmateChecker(board, new StepChecker(board));
    }

    @Test
    void testHasCheck() {
        init();
        when(board.get(5, 0)).thenReturn(
            Optional.of(new ChessPiece(WHITE, KING))
        );
        when(board.get(0, 0)).thenReturn(
            Optional.of(new ChessPiece(BLACK, ROOK))
        );
        assertTrue(checkmateChecker.hasCheck(WHITE));
    }

    @Test
    void testDoesNOtHaveCheck() {
        init();
        when(board.get(5, 0)).thenReturn(
            Optional.of(new ChessPiece(WHITE, KING))
        );
        when(board.get(0, 1)).thenReturn(
            Optional.of(new ChessPiece(BLACK, ROOK))
        );
        assertFalse(checkmateChecker.hasCheck(WHITE));
    }

    @Test
    void testHasCheckmate() {
        init();
        when(board.get(5, 0)).thenReturn(
            Optional.of(new ChessPiece(WHITE, KING))
        );
        when(board.get(0, 0)).thenReturn(
            Optional.of(new ChessPiece(BLACK, ROOK))
        );
        when(board.get(0, 1)).thenReturn(
            Optional.of(new ChessPiece(BLACK, ROOK))
        );
        assertTrue(checkmateChecker.hasCheck(WHITE));
    }

    @Test
    void testDoesNotHaveCheckMate() {
        init();
        when(board.get(5, 0)).thenReturn(
            Optional.of(new ChessPiece(WHITE, KING))
        );
        when(board.get(0, 0)).thenReturn(
            Optional.of(new ChessPiece(BLACK, ROOK))
        );
        assertFalse(checkmateChecker.hasCheckmate(WHITE));
    }

}
