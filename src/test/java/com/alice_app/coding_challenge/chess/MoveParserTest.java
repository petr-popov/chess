package com.alice_app.coding_challenge.chess;

import com.alice_app.coding_challenge.chess.exception.WrongCommandException;
import com.alice_app.coding_challenge.chess.model.Move;
import com.alice_app.coding_challenge.chess.model.CommandParser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.BLACK;
import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.WHITE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MoveParserTest {

    @ParameterizedTest
    @CsvSource(value = {
        "e2=Command should be in 'e2 e4' format",
        "x2=Command should be in 'e2 e4' format",
        "e2 e4 e4=Command should be in 'e2 e4' format",
        "e12 e7=Command should be in 'e2 e4' format",
        "e7 e12=Command should be in 'e2 e4' format",
        "x2 x4=Wrong coordinate 'x', valid options: [a, b, c, d, e, f, g, h]",
        "e2 e0=Wrong coordinate '0', valid options: [1, 2, 3, 4, 5, 6, 7, 8]",
        "e2 e9=Wrong coordinate '9', valid options: [1, 2, 3, 4, 5, 6, 7, 8]"
    }, delimiter = '=')
    void testWrongCommand1(String command, String message) {
        WrongCommandException thrown = assertThrows(WrongCommandException.class,
            () -> CommandParser.parse(WHITE, command));

        assertEquals(message, thrown.getMessage());
    }

    @Test
    void testSuccessfulParsing() {
        Move move = CommandParser.parse(WHITE, "a2 b4");
        assertEquals(WHITE, move.getPlayerPiecesColor());
        assertEquals(0, move.getFromX());
        assertEquals(1, move.getFromY());
        assertEquals(1, move.getToX());
        assertEquals(3, move.getToY());
    }

    @Test
    void testSuccessfulParsingColor() {
        Move move = CommandParser.parse(BLACK, "a2 b4");
        assertEquals(BLACK, move.getPlayerPiecesColor());
    }
}
