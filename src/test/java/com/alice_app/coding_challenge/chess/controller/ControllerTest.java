package com.alice_app.coding_challenge.chess.controller;

import com.alice_app.coding_challenge.chess.exception.WrongCommandException;
import com.alice_app.coding_challenge.chess.model.CheckmateChecker;
import com.alice_app.coding_challenge.chess.model.CommandExecutor;
import com.alice_app.coding_challenge.chess.view.BoardView;
import com.alice_app.coding_challenge.chess.view.Terminal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static com.alice_app.coding_challenge.chess.model.domain.PieceColor.WHITE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;

class ControllerTest {

    @Mock
    private Terminal terminalMock;

    @Mock
    private BoardView boardViewMock;

    @Mock
    private CommandExecutor commandExecutorMock;

    @Mock
    private CheckmateChecker checkmateChecker;

    private Controller controller;

    @BeforeEach
    void init() {
        initMocks(this);

        when(boardViewMock.formatBoard()).thenReturn("white");

        controller = new Controller(terminalMock,
            boardViewMock,
            commandExecutorMock,
            checkmateChecker);

    }

    @Test
    void testFirstInteraction() {
        init();

        assertTrue(controller.firstPlayer);

        controller.askUserAndProcess();

        assertTerminalInvocation(terminalMock, "white", "Player 1: ");

        assertFalse(controller.firstPlayer);
    }

    @Test
    void testSecondInteraction() {
        controller.firstPlayer = false;

        controller.askUserAndProcess();

        assertTerminalInvocation(terminalMock, "white", "Player 2: ");

        assertFalse(controller.isGameCompleted());
        assertTrue(controller.firstPlayer);
    }

    @Test
    void testExceptionHandling() {
        doThrow(new WrongCommandException("error message"))
            .when(commandExecutorMock).execute(any(), any());

        controller.askUserAndProcess();

        assertTerminalInvocation(terminalMock,
            "white", "Player 1: ", "error message");

        assertTrue(controller.firstPlayer);
    }

    @Test
    void testCheck() {
        when(checkmateChecker.hasCheck(WHITE)).thenReturn(true);
        controller.askUserAndProcess();
        assertTerminalInvocation(terminalMock,
            "white", "Player 1 you check", "Player 1: ");
    }

    @Test
    void testCheckmate() {
        when(checkmateChecker.hasCheckmate(WHITE)).thenReturn(true);
        controller.askUserAndProcess();
        assertTerminalInvocation(terminalMock,
            "white", "Player 1 you checkmate\nPlayer 2, congratulations!!!");
    }

    private void assertTerminalInvocation(Terminal terminalMock, String... messages) {
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(terminalMock, times(messages.length)).print(captor.capture());
        assertArrayEquals(
            messages,
            captor.getAllValues().toArray()
        );
    }
}
