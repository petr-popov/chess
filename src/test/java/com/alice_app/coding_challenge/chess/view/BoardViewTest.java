package com.alice_app.coding_challenge.chess.view;

import com.alice_app.coding_challenge.chess.model.domain.Board;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.util.Objects;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BoardViewTest {

    @Test
    void testInitialPositionForWhite() {
        Board board = new Board();
        String expected = read();
        assertEquals(expected, new BoardView(board).formatBoard());
    }

    private String read() {
        InputStream stream = Objects.requireNonNull(
            getClass().getClassLoader().getResourceAsStream("initial_board_for_white.txt")
        );
        return new Scanner(stream, "UTF-8")
            .useDelimiter("\\A")
            .next();
    }
}
