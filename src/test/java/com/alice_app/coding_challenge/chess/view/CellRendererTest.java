package com.alice_app.coding_challenge.chess.view;

import com.alice_app.coding_challenge.chess.model.domain.ChessPiece;
import com.alice_app.coding_challenge.chess.model.domain.PieceType;
import com.alice_app.coding_challenge.chess.model.domain.PieceColor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class CellRendererTest {

    @Test
    void testPieceRendering() {
        ChessPiece piece = new ChessPiece(PieceColor.BLACK, PieceType.BISHOP);
        String[] render = CellRenderer.renderPiece(piece);
        assertArrayEquals(new String[]{
            "        |",
            " BLACK  |",
            " BISHOP |",
            "________|"
        }, render);
    }

    @Test
    void testLabelRendering() {
        String[] render = CellRenderer.renderLabel('a');
        assertArrayEquals(new String[]{
            "        |",
            "   a    |",
            "        |",
            "________|"
        }, render);
    }
}
